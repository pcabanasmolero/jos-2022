# JoS-2022 results

Example results for the paper ***"The Music Demixing Machine: towards real-time remixing of classical music"*** by Cabañas-Molero et al., submitted to The Journal of Supercomputing.

`Allegri` and `Mozart` files are taken from the University of Rochester Multimodal Music Performance (URMP) database.
`Brahms` and `Webern` recordings are property of Odratek Records.
